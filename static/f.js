/**
Send Ajax JSON request.

@param cb Callback function.
@param method Select method of request, i.e. "GET", "POST".
@param path Set path (url) of request.
@param body If "POST" is used, sent `body` data in JSON format.
*/
function send_ajax(cb, method, path, body)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                cb(JSON.parse(xhr.responseText));
            } else {
                console.log(xhr.responseText);
            }
        }
    };
    xhr.open(method, path, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    if (body)
        xhr.send(JSON.stringify(body));
    else
        xhr.send();
}

/**
Request Trello for mapathon cards.

This function gets every card that is not template from *Missing Maps CZ & SK*
board, *MAPATHONY budoucí | Oncoming events* list. Trello returns list of
cards. Each card is converted to `Mapathon` object, stored in new list and then
send to `update_p` function.
*/
function get_mapathons()
{
    send_ajax(
        function (rt) {
            var l = [];
            for (i in rt) {
                if (!rt[i]["labels"][0] ||
                        rt[i]["labels"][0]["name"] != "Šablona")
                    l.push(new Mapathon(rt[i]));
            }
            update_p(l);
        },
        "GET",
        "https://api.trello.com/1/lists/5952cb489f36ed8565925d34/cards"
    );
}

/**
Request Trello for members of the card.
*/
function get_core(cid)
{
    send_ajax(
        function(rt) {
            var m = new Mapathon(rt);
            document.getElementById("dorg").innerHTML = m.getMembersHTML();
        },
        "GET",
        "https://api.trello.com/1/cards/" + cid
    );
}

/**
Update content of paragraph in web page with `list` content.

@param list The list of `Mapathon` objects.
*/
function update_p(list)
{
    msg = "";
    for (obj in list) {
        msg += list[obj].getHTML();
        msg += "<br />";
    }
    document.getElementById("p").innerHTML = msg;
}

/**
The class containing information about mapathon.

@param card The card received from Trello.
*/
function Mapathon(card) {
    this.id = card["id"]
    this.name = card["name"];
    this.where = card["name"].match(/^[^ ]*/);
    this.when = new Date(card["badges"]["due"]);
    this.members = [];
    for (m in card["idMembers"])
        this.members.push(new Member(card["idMembers"][m]));
    this.url = card["shortUrl"];
    this.msg = card["desc"].match(/[^#]*/);
}
Mapathon.prototype = {
    constructor: Mapathon,
    getText: function() {},
    getHTML: function() {
        var html;
        html = "<h2>" + this.getWhere();
        if (this.name.toLowerCase().includes("josm"))
            html += " (JOSM)";
        html += ", " + this.getWhen() + "</h2>";
        html += "<p>";
        for (m in this.members) {
            html += this.members[m].getHTML();
            this.members[m].updateAvatar();
        }
        html += "</p>";
        html += "<p>" + this.msg + "</p>";
        html += "<p><ul>";
        if (this.name.toLowerCase().includes("josm")) {
            html += "<li>" + this.getLinkHTML("reg") + "</li>";
        }
        html += "<li>" + this.getLinkHTML("trello") + "</li>";
        html += "<li>" + this.getLinkHTML("facebook") + "</li>";
        html += "<li>" + this.getLinkHTML("twitter") + "</li>";
        html += "<li>" + this.getLinkHTML("email") + "</li>";
        html += "</ul></p>";
        return html;
    },
    getMembersHTML: function() {
        var html = "";
        for (m in this.members) {
            html += this.members[m].getHTML();
            this.members[m].updateAvatar();
        }
        return html;
    },
    getWhere: function() {
        var wh = this.where + "";
        var wh1 = wh.charAt(0).toUpperCase();
        var wh2 = wh.slice(1).toLowerCase();
        return wh1 + wh2;
    },
    getWhen: function() {
        return this.when.toLocaleString("cs-CZ").slice(0,-3);
    },
    getWhenDay: function() {
        return this.when.toLocaleDateString("cs-CZ");
    },
    getTitle: function() {
        if (this.name.toLowerCase().includes("josm"))
            return "JOSM Mapathon " + this.getWhere() + ", " + this.getWhen();
        return "Mapathon " + this.getWhere() + ", " + this.getWhen();
    },
    getBody: function(newline) {
        var body = "Je tu další mapathon!" + newline;
        body += "- Město: " + this.getWhere() + newline;
        body += "- Datum: " + this.getWhen() + newline;
        body += newline;
        body += "Odkaz: " + this.url;
        return body;
    },
    getEmailBody: function() {
        return this.getBody("%0D%0A");
    },
    getEmailSubj: function() {
        return "Mapathon " + this.getWhere() + ", " + this.getWhenDay();
    },
    getFacebookLink: function() {
        var fb_link = "https://www.facebook.com/sharer/sharer.php?";
        fb_link += "u=" + this.url;
        fb_link += "&title=" + this.getTitle();
        return fb_link;
    },
    getTwitterLink: function() {
        var tw_link = "https://twitter.com/share?";
        tw_link += "url=" + this.url;
        tw_link += "&text=" + this.getTitle();
        return tw_link;
    },
    getEmailLink: function() {
        var eml_link = "mailto:?";
        eml_link += "subject=" + this.getEmailSubj();
        eml_link += "&body=" + this.getEmailBody();
        return eml_link;
    },
    getRegLink: function() {
        return "http://prijdu.mapathon.cz/" + this.id;
    },
    /**
    Return HTML `<a>` tag.

    @param what Specify link type, i.e. "facebook", "twitter", "email".
    */
    getLinkHTML: function(what) {
        if (what == "")
            return null;
        var link;
        var msg;
        switch (what) {
        case "trello":
            link = this.url;
            msg = "Odkaz na Trello";
            break;
        case "facebook":
           link = this.getFacebookLink();
           msg = "Sdílej na Facebooku";
           break;
        case "twitter":
            link = this.getTwitterLink();
            msg = "Sdílej na Twitteru";
            break;
        case "email":
            link = this.getEmailLink();
            msg = "Sdílej emailem";
            break;
        case "reg":
            link = this.getRegLink();
            msg = "Registrace zde";
        }
        return "<a href='" + link + "' target='_blank'>" + msg + "</a>";
    }
}

/**
The class containing Trello member.

@param id The identifier of Trello member (user).
*/
function Member(id) {
    this.id = id;
}
Member.prototype = {
    constructor: Member,
    getHTML: function() {
        return "<img name='" + this.id + "' width='50' height='50' />";
    },
    updateAvatar: function() {
        var thisid = this.id;
        send_ajax(
            function (rt) {
                var mmm = "http://www.missingmaps.org/";
                mmm += "assets/graphics/content/process/Mapathon.svg";
                var dm = document.getElementsByName(thisid);
                for (m in dm) {
                    if (rt["avatarUrl"])
                        dm[m].src = rt["avatarUrl"] + "/original.png";
                    else
                        dm[m].src = mmm;
                }
            },
            "GET",
            "https://api.trello.com/1/members/" + this.id
        );
    }
}
