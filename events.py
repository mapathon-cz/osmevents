from datetime import datetime
from datetime import timedelta
from dateutil import parser, tz
from flask import Flask, redirect, request, session, url_for
from flask import make_response
from flask import render_template, send_from_directory
from flask_oauthlib.client import OAuth
from icalendar import Calendar, Event
from json import loads
from re import search
from requests import get
from xml.etree import ElementTree

from conf import Conf
from db import Db
from lang.cs import LGEVENT, LMEVENT

cfg = Conf()
osm_cfg = cfg.getOSMOAuth()
db = Db(cfg.getEventsDbPath())

oauth = OAuth()
osm = oauth.remote_app(
    name="osm",
    base_url=osm_cfg["base_url"],
    consumer_key=osm_cfg["consumer_key"],
    consumer_secret=osm_cfg["consumer_secret"],
    request_token_url=osm_cfg["request_token_url"],
    access_token_url=osm_cfg["access_token_url"],
    authorize_url=osm_cfg["authorize_url"],
)

@osm.tokengetter
def get_osm_token(token=None):
    return session.get("osm_token")

app = Flask(__name__)
app.secret_key = cfg.getFlaskSecretKey()

@app.route("/")
def root():
    return redirect(url_for("listfuture"))

@app.route("/static/<fn>")
def send_static(fn):
    return send_from_directory("static", fn)

@app.route("/auth")
def auth():
    return osm.authorize(callback=url_for(
        "authed",
        event=request.args.get("event"),
    ))

@app.route("/authed")
def authed():
    resp = osm.authorized_response()
    if resp is None:
        return "Auth failed."
    session["osm_token"] = (
        resp["oauth_token"],
        resp["oauth_token_secret"],
    )
    try:
        resp = osm.get("user/details")
    except:
        return redirect(url_for("auth", event=event))
    try:
        xml_user = resp.data.find("user")
        xml_dn = xml_user.get("display_name")
        session["osm_user"] = xml_dn
        try:
            xml_img = xml_user.find("img")
            xml_avatar_url = xml_img.get("href")
        except:
            xml_avatar_url = False
    except:
        return "Cannot get username."
    db.updateUser(
        session["osm_user"],
        xml_avatar_url,
        *session["osm_token"],
    )
    return redirect(url_for("get_event", event=request.args.get("event")))

@app.route("/user", methods=('GET', 'POST'))
def manage_user():
    if not "osm_user" in session:
        return redirect(url_for("auth", event="/user"))
    if request.method == "POST":
        db.updateUser(
            session["osm_user"],
            request.form["email"],
            *session["osm_token"],
        )
    user = db.getUser(session["osm_user"])
    return render_template("manage_user.cs", user=user)

@app.route("/deleteuser")
def delete_user():
    if not "osm_user" in session:
        return redirect(url_for("auth", event="/deleteuser"))
    res = db.deleteUser(session["osm_user"])
    session.clear()
    return "user deleted" if res else "problem with delete"

@app.route("/listpassed")
def listpassed():
    events = db.getEvents()
    return render_template("get_events.html", events=events, L=LGEVENT)

@app.route("/listfuture")
def listfuture():
    events = db.getEvents(False)
    return render_template("get_events.html", events=events, L=LGEVENT)

@app.route("/listall")
def listall():
    events = db.getEvents(False)
    events += db.getEvents()
    return render_template("get_events.html", events=events, L=LGEVENT)

def event_to_vevent(ev):
    e = Event()
    e.add("dtstart", ev["due"].astimezone(tz.tzlocal()))
    e.add("dtend", ev["due"].astimezone(tz.tzlocal()) + timedelta(hours=3))
    e.add("summary", ev["name"])
    if ev["desc"] is not None and ev["desc"] is not "":
        e.add("description", ev["desc"])
    if ev["url"] and "http" in ev["url"]:
        e.add("url", ev["url"])
    try:
        r = get("https://www.openstreetmap.org/api/0.6/node/{}".format(
            ev["loc"]
        ))
        et = ElementTree.fromstring(r.text)
        ev["osmnode"] = {
            "id": et.findall("node")[0].get("id"),
            "lat": et.findall("node")[0].get("lat"),
            "lon": et.findall("node")[0].get("lon"),
        }
        for i in et.findall("node")[0].findall("tag"):
            ev["osmnode"][i.get("k").replace(":", "")] = i.get("v")
        e.add("geo", (
            ev["osmnode"]["lat"],
            ev["osmnode"]["lon"],
        ))
        e.add("location", "{} {}, {}".format(
            ev["osmnode"]["addrstreet"],
            ev["osmnode"]["addrhousenumber"],
            ev["osmnode"]["addrcity"],
        ))
    except:
        pass
    return e

@app.route("/future")
@app.route("/future.ics")
@app.route("/future/manage")
@app.route("/future.ics/manage")
def ical_future():
    c = Calendar()
    c.add("version", "2.0")
    c.add("prodid", "-//qeef//osmevents//EN")
    for ev in db.getEvents():
        c.add_component(event_to_vevent(ev))
    for ev in db.getEvents(False):
        c.add_component(event_to_vevent(ev))
    r = make_response(c.to_ical())
    r.headers["Content-Disposition"] = "attachment; filename=future.ics"
    return r

@app.route("/<event>.ics")
def ical_event(event):
    c = Calendar()
    c.add("version", "2.0")
    c.add("prodid", "-//qeef//osmevents//EN")
    try:
        c.add_component(event_to_vevent(db.getEvent(event)))
    except:
        return redirect(url_for("get_event", event=event))
    r = make_response(c.to_ical())
    r.headers["Content-Disposition"] = "attachment; filename=event.ics"
    return r

@app.route("/<event>")
def get_event(event):
    try:
        ev = db.getEvent(event)
        ev["osmnode"] = None
        ct = datetime.now(tz.tzlocal())
        passed = True if ev["due"] < ct else False
        if "osm_user" in session:
            enrolled = db.getEnrolled(ev["eid"], session["osm_user"])
        else:
            enrolled = False
        try:
            r = get("https://www.openstreetmap.org/api/0.6/node/{}".format(
                ev["loc"]
            ))
            et = ElementTree.fromstring(r.text)
            ev["osmnode"] = {
                "id": et.findall("node")[0].get("id"),
                "lat": et.findall("node")[0].get("lat"),
                "lon": et.findall("node")[0].get("lon"),
            }
            for i in et.findall("node")[0].findall("tag"):
                ev["osmnode"][i.get("k").replace(":", "")] = i.get("v")
        except:
            pass
    except:
        ev = {}
        enrolled = False
        passed = False
    return render_template(
        "get_event.html",
        ev=ev,
        avatars=db.getEmails(event),
        enrolled=enrolled,
        passed=passed,
        L=LGEVENT,
    )

@app.route("/<event>.ics/manage")
def manage_event_ics(event):
    return redirect(url_for("get_event", event=event))

@app.route("/<event>/manage", methods=('GET', 'POST'))
def manage_event(event):
    if not "osm_user" in session:
        return redirect(url_for("auth", event="{}/manage".format(event)))
    if request.method == "POST":
        fsizes = {
            "t": {},
            "s": {},
        }
        for (k, v) in request.form.items():
            if "_" in k:
                fsizes[k.split("_")[0]][k.split("_")[1]] = v
        sizes = []
        for (k, v) in fsizes["t"].items():
            if fsizes["t"][k] != "" and fsizes["s"][k] != "":
                try:
                    sizes.append((fsizes["t"][k], int(fsizes["s"][k])))
                except:
                    sizes.append((fsizes["t"][k], 0))
        db.updateEvent(
            event,
            request.form["name"],
            parser.parse(request.form["due"]).replace(tzinfo=tz.tzlocal()),
            request.form["loc"],
            request.form["url"],
            request.form["desc"],
            request.form["note"],
        )
        db.deleteSizes(event)
        for (t, s) in sizes:
            db.createSize(event, t, s)
    try:
        ev = db.getEvent(event)
    except:
        ev = False
    if ev is False:
        try:
            r = get(
                "https://api.trello.com/1/cards/{}".format(event),
                headers={"Content-Type": "application/json"},
            )
            card = loads(r.text)
            # temporary for JOSM mapathons only
            if "josm" not in card["name"].lower():
                raise ValueError
            name = card["name"].split("|")
            ev = cfg.getDefaultEvent()
            ev["eid"] = event # same as r["id"]
            if len(name) > 2 and "josm" in name[2].strip(" ").lower():
                ev["name"] = "JOSM Mapathon"
            else:
                ev["name"] = "Mapathon"
            ev["name"] += " {}".format(name[0].strip(" "))
            ev["due"] = parser.parse(card["badges"]["due"])
            ev["due"] = ev["due"].astimezone(tz.tzlocal())
            ev["loc"] = name[1].strip(" ")
            ev["url"] = "{}{}".format(request.url_root, event)
            ev["desc"] = search("[^#]*", card["desc"]).group(0).strip()
            if len(name) > 2 and "josm" in name[2].strip(" ").lower():
                ev["sizes"] = [["JOSM", 15, 0]]
            else:
                ev["sizes"] = cfg.getDefaultEvent()["sizes"]
        except:
            ev = False
    if ev is False:
        ev = cfg.getDefaultEvent()
        ev["eid"] = event
        ev["due"] = datetime.now(tz.tzlocal())
        ev["url"] = "{}{}".format(request.url_root, event)
    return render_template("manage_event.html", ev=ev, L=LMEVENT)

@app.route("/<event>/enroll")
def enroll_event(event):
    """Enroll the user to the event.

    Keyword arguments:
    event -- Event identifier.
    """
    if not "osm_user" in session:
        return redirect(url_for("auth", event="{}/enroll".format(event)))
    db.enroll(event, session["osm_user"], request.args.get("t"))
    return redirect(url_for("get_event", event=event))

@app.route("/<event>/unenroll")
def unenroll_event(event):
    """Unenroll the user from the event.

    Keyword arguments:
    event -- Event identifier.
    """
    if not "osm_user" in session:
        return redirect(url_for("auth", event="{}/unenroll".format(event)))
    db.unenroll(event, session["osm_user"])
    return redirect(url_for("get_event", event=event))

@app.route("/<event>/listusers")
def listusers_event(event):
    """List the usernames that are enrolled to the event.

    Keyword arguments;
    event -- Event identifier.
    """
    if not "osm_user" in session:
        return redirect(url_for("auth", event="{}/listusers".format(event)))
    sizes = db.getSizes(event)
    users = db.getUsers(event)
    return render_template(
        "listusers_event.html",
        sizes=sizes,
        users=users,
    )
