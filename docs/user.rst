User's Guide
============

The link to an event is ``http://DOMAIN/EVENTIDENTIFIER`` where ``DOMAIN`` is
from :ref:`install` section.

Here is the list of forbidden event identifiers (``EVENTIDENTIFIER``). These
are forbidden because of special purpose:

- ``auth`` is used for authentication.
- ``authed`` is used for authentication.
- ``user`` is used for user management.
- ``deleteuser`` removes all the stored information about user.
- ``listpassed`` shows the list of already passed events.

Any other string may be used as event identifier.

Enroll to event
---------------

There is a link to enroll to category at events link (the format of the link is
``http://DOMAIN/EVENTIDENTIFIER``).

Manage event
------------

To manage event, add ``/manage`` to the link of event (the format of the link
is ``http://DOMAIN/EVENTIDENTIFIER/manage``).

To list users, add ``/listusers`` to the link of event (the format of the link
is ``http://DOMAIN/EVENTIDENTIFIER/listusers``).

To list emails of users, add ``/listemails`` to the link of event (the format
of the link is ``http://DOMAIN/EVENTIDENTIFIER/listemails``). This shows only
emails that are filled by user.

Manage user
-----------

To be deleted from database or add email address as contact, use ``user`` as
``EVENTIDENTIFIER`` (i.e. ``http://DOMAIN/user``).
