Deployment Howto
================

.. _install:

Installation
------------

Clone repository, rename ``master`` branch as ``deploy``, update config file with
Flask app secret key, and consumer key and secret. Commit these changes to
``deploy`` branch. *DO NOT UPLOAD THIS COMMIT!*

Create virtual environment and install the requirements::

   virtualenv -p python3 venv
   . venv/bin/activate
   pip install -r requirements.txt
   deactivate

When updating to the newest version, i.e. ``v0.3.0``, just ``git fetch`` and ``git
rebase v0.3.0`` while on ``deploy`` branch.

`Deploy with NGINX and uWSGI`_. NGINX configuration::

   server {
           listen 80;
           listen [::]:80;

           server_name DOMAIN;

           root /var/www/osmevents;
           index index.html;

           location / {
                   try_files $uri @osmevents;
           }

           location @osmevents {
                   include uwsgi_params;
                   uwsgi_pass unix:/tmp/osmevents.sock;
           }
   }

Configure `systemd`_, ``/etc/systemd/system/osmevents.socket``::

   [Unit]
   Description=Socket for osmevents

   [Socket]
   ListenStream=/tmp/osmevents.sock
   SocketUser=www-data
   SocketGroup=www-data
   SocketMode=0660

   [Install]
   WantedBy=sockets.target

and ``/etc/systemd/system/osmevents.service``::

   [Unit]
   Description=osmevents
   After=syslog.target

   [Service]
   ExecStart=/usr/bin/uwsgi \
       --uid www-data \
       --socket /tmp/osmevents.sock \
       --manage-script-name \
       --mount /osmevents=events:app \
       --plugin python3 \
       --virtualenv /var/www/osmevents/venv \
       --chdir /var/www/osmevents
   User=www-data
   Group=www-data
   Restart=on-failure
   KillSignal=SIGQUIT
   Type=notify
   StandardError=syslog
   NotifyAccess=all

Enable systemd socket/service with ``systemctl enable osmevents.socket`` and
``systemctl enable osmevents.service``. When upgraded to the newest version,
restart the service with the ``systemctl restart osmevents.service``.

.. _deploy with NGINX and uWSGI: http://flask.pocoo.org/docs/1.0/deploying/uwsgi/
.. _systemd: https://uwsgi-docs.readthedocs.io/en/latest/Systemd.html

Configuration
-------------

All the settings is handled in the ``/path/to/osmevents/.cfg`` file.

``default_event``
   When creating new event, these are default values. ``sizes`` is the list
   with entry ``["category", capacity]``.

``flask_app``
   Run the command in ``secret_key``. It generates random number. Use it for
   secret key of Flask app.

``events_db``
   The path to database file is set with ``path``.

``osm_oauth``
   There has to be ``consumer_key`` and ``consumer_secret`` set. It can be
   generated at `osm.org`_ in *My Settings* -> *oauth settings* -> *Register
   your application* (bottom of page).

.. _osm.org: https://www.openstreetmap.org/
