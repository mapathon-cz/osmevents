Welcome to osmevents's documentation
====================================

The `osmevents <https://gitlab.com/qeef/osmevents>`_ project is inspired by the
need of a management system for user registrations when organizing mapathons.
It enables you to create event (like mapathon) with categories (like iD, JOSM,
validation) and let the users (authenticated over `osm.org
<https://www.openstreetmap.org/>`_) enroll. All of that as a web application
based on `Flask <http://flask.pocoo.org/>`_.

.. toctree::
   :maxdepth: 2

   admin
   user
   api
