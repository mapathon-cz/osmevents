from dateutil.parser import parse
from sqlite3 import connect

class FileNotSetError(ValueError):
    pass

class ArgCountError(ValueError):
    pass

class ValueNotSetError(ValueError):
    pass

class NotInDbError(ValueError):
    pass

def conn(fn):
    """Open/close db connection decorator."""
    def fw(self, *args, **kwargs):
        if self.dbpath:
            self.con = connect(self.dbpath)
        else:
            self.con = None
            raise FileNotSetError("Database file must be set")
        self.cur = self.con.cursor()
        ev = fn(self, *args, **kwargs)
        # TODO self.con.close()
        return ev
    return fw

class Db:
    """Database access."""
    def __init__(self, dbpath=False):
        self.dbpath = dbpath
        if dbpath:
            self.con = connect(dbpath)
        else:
            self.con = None
            raise FileNotSetError("Database file must be set")
        self.cur = self.con.cursor()
        self.con.close()
        return None

    def __del__(self):
        try:
            if self.con:
                self.con.close()
        except:
            pass
        return

    @conn
    def createDb(self):
        """Create database of events."""
        que = """
            PRAGMA foreign_keys = ON;

            CREATE TABLE IF NOT EXISTS "users" (
                name TEXT PRIMARY KEY NOT NULL,
                email TEXT,
                token TEXT,
                secret TEXT,
                UNIQUE(email)
            );

            CREATE TABLE IF NOT EXISTS "events" (
                eid TEXT PRIMARY KEY NOT NULL,
                name TEXT,
                due TEXT,
                loc TEXT,
                url TEXT,
                desc TEXT,
                note TEXT
            );

            CREATE TABLE IF NOT EXISTS "sizes" (
                event TEXT NOT NULL REFERENCES events(eid),
                type TEXT NOT NULL,
                size INTEGER DEFAULT 0,
                UNIQUE(event, type, size)
            );

            CREATE TABLE IF NOT EXISTS "enroll" (
                event TEXT NOT NULL REFERENCES events(eid),
                user TEXT NOT NULL REFERENCES users(name),
                type TEXT NOT NULL REFERENCES sizes(type),
                UNIQUE(event, user)
            );
        """
        self.cur.executescript(que)
        self.con.commit()
        return

    @conn
    def updateEvent(
        self,
        eid=None,
        name="",
        due="",
        loc="",
        url="",
        desc="",
        note=""
    ):
        """Update event in db.

        Keyword arguments:
        eid -- The event identifier.
        name -- The name of the event.
        due -- The due date/time of the event.
        loc -- The location of the event.
        url -- The URL that links to the event info.
        desc -- The description of the event.
        note -- The note about the event.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = """
            INSERT OR REPLACE INTO events
            (name, due, loc, url, desc, note, eid)
        """
        que += "VALUES ("
        if name is "":
            que += "(SELECT name FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(name)
        if due is "":
            que += "(SELECT due FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(due)
        if loc is "":
            que += "(SELECT loc FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(loc)
        if url is "":
            que += "(SELECT url FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(url)
        if desc is "":
            que += "(SELECT desc FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(desc)
        if note is "":
            que += "(SELECT note FROM events WHERE eid='{}'), ".format(eid)
        else:
            que += "'{}', ".format(note)
        que += "'{}')".format(eid)
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def createSize(self, eid=None, t=None, s=None):
        """Create size of type of event in db.

        Keyword argumnts:
        eid -- The event identifier.
        t -- The type of entrollment.
        s -- The max number of users for type.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        if t is None:
            raise ValueNotSetError("t must be set")
        if s is None:
            raise ValueNotSetError("s must be set")
        que = "INSERT INTO sizes VALUES('{}', '{}', '{}')".format(eid, t, s)
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def deleteSizes(self, eid=None):
        """Delete size of type of event from db.

        Keyword argumnts:
        eid -- The event identifier.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = "DELETE FROM sizes WHERE event='{}'".format(eid)
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def getEvents(self, passed=True):
        """Return events based on ``passed`` variable.

        Keyword arguments:
        passed -- If True then return events older than today.
        """
        if passed:
            que = """
                SELECT * FROM 'events'
                WHERE due < datetime(CURRENT_TIMESTAMP, 'localtime')
            """
        else:
            que = """
                SELECT * FROM 'events'
                WHERE due >= datetime(CURRENT_TIMESTAMP, 'localtime')
            """
        events = []
        for e in self.cur.execute(que):
            events.append({
                "eid": e[0],
                "name": e[1],
                "due": parse(e[2]),
                "loc": e[3],
                "url": e[4],
                "desc": e[5],
                "note": e[6],
            })
        return events

    @conn
    def getEvent(self, eid=None):
        """Get event from db.

        Keyword arguments:
        eid -- The event identifier.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = "SELECT * FROM 'events' WHERE eid='{}'".format(eid)
        event = {}
        try:
            e = self.cur.execute(que).fetchone()
            event["eid"] = e[0] if e[0] else eid
            event["name"] = e[1]
            event["due"] = parse(e[2])
            event["loc"] = e[3]
            event["url"] = e[4]
            event["sizes"] = self.getSizes(eid)
            event["desc"] = e[5]
            event["note"] = e[6]
        except:
            raise NotInDbError("event not in database")
        return event

    @conn
    def getSizes(self, eid=None):
        """Return list of types and sizes for the event.

        Keyword arguments:
        eid -- The event identifier.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = """
            SELECT sizes.type, sizes.size, COUNT(enroll.event) FROM 'sizes'
            LEFT JOIN 'enroll' USING(event, type)
            WHERE sizes.event='{}'
            GROUP BY type
        """.format(eid)
        sizes = []
        for e in self.cur.execute(que):
            sizes.append(e)
        return sizes

    @conn
    def updateUser(self, name=None, email="", token="", secret=""):
        """Update user in db.

        Keyword arguments:
        name -- The name of the user.
        email -- The email of the user.
        token -- The authorization token.
        secret -- The authorization token secret.
        """
        if name is None:
            raise ValueNotSetError("name must be set")
        if email is False:
            que = """
                INSERT OR REPLACE INTO users (name, email, token, secret)
                VALUES (
                    '{}',
                    (SELECT email FROM users WHERE name='{}'),
                    '{}',
                    '{}'
                )
            """.format(
                name,
                name,
                token,
                secret,
            )
        else:
            que = """
                UPDATE 'users' SET
                    email='{}',
                    token='{}',
                    secret='{}'
                WHERE name='{}'
            """.format(
                email,
                token,
                secret,
                name,
            )
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def getUser(self, name=None):
        """Get user from db.

        Keyword arguments:
        name -- The name of the user.
        """
        if name is None:
            raise ValueNotSetError("name must be set")
        que = "SELECT * FROM 'users' WHERE name='{}'".format(name)
        user = {}
        u = self.cur.execute(que).fetchone()
        self.con.commit()
        user["name"] = u[0]
        user["email"] = u[1]
        user["token"] = u[2]
        user["secret"] = u[3]
        return user

    @conn
    def getUsers(self, eid=None):
        """Get users enrolled to event.

        Keyword arguments:
        eid -- The event identifier.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = """
            SELECT user, type FROM enroll
            WHERE event='{}'
            ORDER BY type, user
        """.format(eid)
        users = []
        for u in self.cur.execute(que):
            users.append((u[0], u[1]))
        return users

    @conn
    def getEmails(self, eid=None):
        """Get emails of users enrolled to event.

        TODO: emails are used for avatars now.

        Keyword arguments:
        eid -- The event identifier.
        """
        if eid is None:
            raise ValueNotSetError("eid must be set")
        que = """
            SELECT email FROM users
            INNER JOIN enroll ON enroll.user=users.name
            WHERE event='{}'
        """.format(eid)
        emails = []
        for e in self.cur.execute(que):
            emails.append(e[0])
        return emails

    @conn
    def getUserTokens(self, name=None):
        """Get OAuth tokens of user from db.

        Keyword arguments:
        name -- The name of the user.
        """
        if name is None:
            raise ValueNotSetError("name must be set")
        que = "SELECT token, secret FROM 'users' WHERE name='{}'".format(name)
        u = self.cur.execute(que).fetchone()
        return u

    @conn
    def deleteUser(self, name=None):
        """Delete user from db.

        Keyword argument:
        name -- The name of the user.
        """
        if name is None:
            raise ValueNotSetError("name must be set")
        que = "DELETE FROM 'users' WHERE name='{}'".format(name)
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        que = "DELETE FROM enroll WHERE user='{}'".format(name)
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def enroll(self, event=None, user=None, t=None):
        """Store in db enrollment of user at event.

        Keyword arguments:
        event -- The event identifier, ``events(id)``.
        user -- The user identifier, ``users(name)``.
        t -- The type of the enrollment, ``size(type)``.
        """
        if event is None:
            raise ValueNotSetError("event must be set")
        if user is None:
            raise ValueNotSetError("user must be set")
        if t is None:
            raise ValueNotSetError("t must be set")
        ev = self.getEvent(event)
        for (et, es, ec) in ev["sizes"]:
            if et == t and es <= ec:
                return False
        que = """
            INSERT INTO 'enroll'
            VALUES('{}', '{}', '{}')
        """.format(
            event,
            user,
            t,
        )
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def unenroll(self, event=None, user=None):
        """Delete the enrollment of user at event.

        Keyword arguments:
        event -- The event identifier, ``events(id)``.
        user -- The user identifier, ``users(name)``.
        """
        if event is None:
            raise ValueNotSetError("event must be set")
        if user is None:
            raise ValueNotSetError("user must be set")
        que = """
            DELETE FROM 'enroll'
            WHERE event='{}' AND user='{}'
        """.format(
            event,
            user,
        )
        succ = True
        try:
            self.cur.execute(que)
            self.con.commit()
        except:
            succ = False
        return succ

    @conn
    def getEnrolled(self, event=None, name=None):
        """Get enrollment info of user to event.

        Keyword arguments:
        event -- The event identifier, ``events(id)``.
        user -- The user identifier, ``users(name)``.
        """
        if event is None:
            raise ValueNotSetError("event must be set")
        if name is None:
            raise ValueNotSetError("name must be set")
        que = """
            SELECT type FROM 'enroll'
            WHERE event='{}' AND user='{}'
        """.format(event, name)
        enrolled = {}
        try:
            e = self.cur.execute(que).fetchone()
            self.con.commit()
            enrolled["type"] = e[0]
        except:
            pass
        return enrolled
