"""Czech language pack."""
LGEVENT = [
["nakonec přijít", "nestihnu"],
"zbývá",
"přihlásit se",
"registrace ještě není",
"už je po",
"Sdílej na Facebooku",
"Sdílej na Twitteru",
"Sdílej emailem",
"Pro přihlášení je potřeba účet OpenStreetMap",
"mapa",
"Přidej do kalendáře",
"iCal kalendář",
]
LMEVENT = [
"Nastavení akce",
"aktualizovat údaje",
"přidat typ registrace a kapacitu",
"název",
"kdy",
"kde (OSM node)",
"odkaz",
"kapacity",
"popis (lze použít HTML tagy)",
"poznámka (lze použít HTML tagy)",
]
