# osmevents
This project is DEPRECATED in the favor of [osmcal.org][11].

[11]: https://github.com/thomersch/openstreetmap-calendar

---

This project is inspired by need of management system for organizing mapathons.
The main benefit is OAuth with [OSM webpage][5].

## License
This project is developed under [three clause BSD license][4].

[4]: ./LICENSE

# Development
- Get ready with requirements
```
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
```

- Change security parameters in `.cfg` file. Particulary, the Flask app secret
  key and consumer key and secret must be changed.

- Update default event.

- Run development
```
FLASK_ENV=development FLASK_APP=events flask run
```

# Deploy
See [Deployment Howto][6] for example configuration.

# Contribute
Use [OneFlow][3] branching model and keep the [changelog][1].

Write [great git commit messages][2]:
1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[1]: ./CHANGELOG.md
[2]: https://chris.beams.io/posts/git-commit/
[3]: http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[5]: https://www.openstreetmap.org/
[6]: https://qeef.gitlab.io/osmevents/admin.html

# Documentation
The project is documented with [Sphynx][8]. When new version tag is uploaded,
the [project documentation][9] is updated automatically (see
[.gitlab-ci.yml][10]).

[8]: http://www.sphinx-doc.org/
[9]: https://qeef.gitlab.io/osmevents/
[10]: ./.gitlab-ci.yml
