# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]
### Fixed
- "None" avatar.

### Deprecated
- THIS REPOSITORY.

## [0.4.1][] - 2019-09-10
### Fixed
- Do not show avatar if there is none.

## [0.4.0][] - 2019-09-09
### Changed
- Show participants avatars instead of organizators.
- Store and use avatar link from OSM.

## [0.3.0][] - 2019-07-31
### Added
- End time to ical vevent.

### Changed
- Restyle events lists.

### Fixed
- Timezone in ical vevent.

## [0.2.0][] - 2019-06-29
### Added
- If Trello used, load avatars of org team.
- Description and note to event.
- List future events.
- Get future events as iCal.
- Get event as iCal.

### Changed
- Use Python3 module as language instead of jinja templates.
- Event design.
    - Show info even for not logged users.
    - Add map and address based on OSM node id.
    - Add social networks sharing links.
- Event manage design.

### Removed
- Unused language templates, unused list emails template.

## [0.1.3][] - 2019-06-05
### Fixed
- List of enrolled users.

## [0.1.2][] - 2019-06-04
### Fixed
- UTC to local time for events when parsing due from Trello.

## [0.1.1][] - 2019-06-03
### Fixed
- GitLab CI/CD.
- Requirements.

## 0.1.0 - 2019-06-02
### Added
- Changelog, license, readme.
- Flask skeleton with OAuth to [OSM](https://www.openstreetmap.org/).
- Database access, events, users, enrollments.
- User management.
- Event management.
- Lists of users/emails of users enrolled to event.
- List of passed events.
- Documentation.

### Fixed
- Disable enroll when event is full.

[Unreleased]: https://gitlab.com/qeef/osmevents/compare/v0.4.1...master
[0.4.1]: https://gitlab.com/qeef/osmevents/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/qeef/osmevents/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/qeef/osmevents/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/qeef/osmevents/compare/v0.1.3...v0.2.0
[0.1.3]: https://gitlab.com/qeef/osmevents/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/qeef/osmevents/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/qeef/osmevents/compare/v0.1.0...v0.1.1
