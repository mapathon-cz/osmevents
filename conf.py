from json import loads
from os.path import dirname, abspath

class Conf:
    """Get configuration from file."""
    PATH = "{}/.cfg".format(dirname(abspath(__file__)))

    def __init__(self):
        with open(Conf.PATH) as f:
            self.conf = loads(f.read())
        return None

    def getDefaultEvent(self):
        """Return default event parameters."""
        ev = dict(self.conf["default_event"])
        for i in ev["sizes"]:
            if len(i) == 2:
                i.append(0)
        return ev

    def getEventsDbPath(self):
        """Return the path to database file."""
        return self.conf["events_db"]["path"]

    def getFlaskSecretKey(self):
        """Return the secret key from config file."""
        return self.conf["flask_app"]["secret_key"]

    def getLang(self):
        """Return language from config."""
        return self.conf["i18n"]["lang"]

    def getOSMOAuth(self):
        """Return directory of OSM OAuth info."""
        return self.conf["osm_oauth"]
