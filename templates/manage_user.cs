{% extends "manage_user.html" %}
{% block info %}
    Následující informace jsou uchovávány v naší databázi. Pokud chcete
    dostávat pozvánky emailem, je nutné, aby v poli email byla vaše adresa.
{% endblock %}
{% block name %}jméno{% endblock %}
{% block email %}email{% endblock %}
{% block update %}aktualizovat údaje{% endblock %}
{% block delete %}chci být smazán z databáze{% endblock %}
